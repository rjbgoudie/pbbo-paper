functions {
  vector pb_model (vector times, real h0, real h1, real s0, real s1, real theta) {
    int N_obs = num_elements(times);
    vector [N_obs] result;

    for (ii in 1 : N_obs) {
      real denom_one = exp(s0 * (times[ii] - theta));
      real denom_two = exp(s1 * (times[ii] - theta));
      real numerator = 2 * (h1 - h0);
      result[ii] = h1 - (numerator / (denom_one + denom_two));
    }

    return(result);
  }
}

data {
  int <lower = 1> N_obs_per_indiv;
  vector <lower = 1, upper = 20> [N_obs_per_indiv] obs_times;
  vector <lower = 0, upper = 250> [N_obs_per_indiv] heights;

  real <lower = 0> h0_mean;
  real <lower = 0> h0_scale;
  real <lower = 0> delta_h1_mean;
  real <lower = 0> delta_h1_scale;

  real <lower = 0> s0_mean;
  real <lower = 0> s0_scale;
  real <lower = 0> delta_s1_mean;
  real <lower = 0> delta_s1_scale;

  real <lower = 0> theta_mean;
  real <lower = 0> theta_scale;
  /* Do not pass these in -- we know that the priors for them are
  essentially random
  real <lower = 0> noise_mean;
  real <lower = 0> noise_scale;
  */
}

transformed data {
  real <lower = 0> min_obs_time = min(obs_times);
  real <lower = 0> max_obs_time = max(obs_times);
}

parameters {
  real <lower = 0> h0;
  real <lower = 0> delta_h1;
  real <lower = 0> s0;
  real <lower = 0> delta_s1;
  real <lower = min_obs_time, upper = max_obs_time> theta;
  real <lower = 0> noise_sd;
}

model {
  vector [N_obs_per_indiv] mu = pb_model(
    obs_times,
    h0,
    h0 + delta_h1,
    s0,
    s0 + delta_s1,
    theta
  );

  target += normal_lpdf(heights | mu, noise_sd);

  target += lognormal_lpdf(h0 | log(h0_mean), h0_scale);
  target += lognormal_lpdf(delta_h1 | log(delta_h1_mean), delta_h1_scale);
  target += lognormal_lpdf(s0 | log(s0_mean), s0_scale);
  target += lognormal_lpdf(delta_s1 | log(delta_s1_mean), delta_s1_scale);
  target += lognormal_lpdf(theta | log(theta_mean), theta_scale);
  target += lognormal_lpdf(noise_sd | 0, 0.2);
  //target += lognormal_lpdf(noise_sd | log(noise_sd_mean), noise_sd_scale);
}
