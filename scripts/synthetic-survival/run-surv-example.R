library(pbbo)

source("scripts/common/setup-logger.R")
source("scripts/common/setup-argparse.R")
source("scripts/synthetic-survival/GLOBALS.R")

parser$add_argument("--censoring-and-covariate-data")
args <- parser$parse_args()

censoring_and_covariate <- readRDS(args$censoring_and_covariate_data)
task_id <- Sys.getenv("SLURM_ARRAY_TASK_ID") %>%
  as.numeric()

censoring_times <- censoring_and_covariate$censoring_times
covariate_mat <- censoring_and_covariate$covariates
full_cov_mat <- cbind(censoring_times, covariate_mat)

local_job_id <- surv_task_tbl[task_id, "job_id"]
local_extra_term <- surv_task_tbl[task_id, "extra_term"]

if (local_extra_term) {
  extra_term <- neg_mean_log_sd
} else {
  extra_term <- NULL
}

pbbo_res <- pbbo(
  target_lcdf = target_lcdf,
  target_sampler = target_sampler,
  prior_predictive_sampler = prior_predictive_sampler,
  param_set = param_set,
  covariate_values = full_cov_mat,
  discrepancy = "log_cvm",
  importance_method = "surv_mixture",
  importance_args = list(
    surv_mixture_sd_multiplier = 1.05,
    surv_mixture_cont_frac = 0.95,
    surv_mixture_cens_times = censoring_times
  ),
  bayes_opt_batches = 1,
  bayes_opt_iters_per_batch = 300,
  bayes_opt_design_points_per_batch = 60,
  n_internal_prior_draws = 1e5,
  n_internal_importance_draws = 5e3,
  bayes_opt_print = TRUE,
  extra_objective_term = extra_term
)

saveRDS(object = pbbo_res, file = args$output)
