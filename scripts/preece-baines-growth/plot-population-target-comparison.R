library(dplyr)
library(tibble)

source("scripts/common/setup-argparse.R")
source("scripts/common/setup-ggplot-theme.R")
source("scripts/preece-baines-growth/GLOBALS.R")

parser$add_argument("--optima-results-long")
args <- parser$parse_args()

optima_long <- readRDS(args$optima_results_long)

n_samples_per_optima <- 5e3
plot_lower <- 50
plot_upper <- 210

# get exact density values for target
target_samples <- tibble(
  x = target_sampler_pop(n = 5 * n_samples_per_optima),
  origin = "target"
)

# draw samples from the optima -- if this is slow push it to a different file
n_rep <- max(optima_long$rep_id)
optima_samples <- lapply(1 : n_rep, function(a_rep_id) {
  lambda <- optima_long %>%
    rep_to_lambda(a_rep_id)

  local_extra_term <- optima_long %>%
    filter(rep_id == a_rep_id) %>%
    pull(extra_term) %>%
    unique()

  local_kappa <- optima_long %>%
    filter(rep_id == a_rep_id) %>%
    pull(kappa) %>%
    unique()

  samples_tbl <- tibble(
    x = full_prior_predictive_sampler_pop(n_samples_per_optima, lambda),
    rep_id = a_rep_id
  ) %>%
    mutate(
      extra_term = local_extra_term,
      kappa = local_kappa
    )

  return(samples_tbl)
}) %>%
  bind_rows()

optima_samples <- optima_samples %>%
  left_join(optima_long %>% select(rep_id, log_discrep), by = "rep_id")

best_kappa_char <- optima_samples %>%
  filter(!is.na(kappa)) %>%
  pull(kappa) %>%
  unique() %>%
  as.character()

optima_samples <- optima_samples %>%
  mutate(
    kappa_fact = ifelse(is.na(kappa), "NA", as.character(kappa)),
    kappa_fact = factor(
      x = kappa_fact,
      levels = c(best_kappa_char, "NA"),
      labels = c(
        sprintf("italic(kappa) == %s", best_kappa_char),
        sprintf("italic(kappa) == 'NA'")
      )
    )
  )

# first version, colour by extra term or not
p1 <- ggplot() +
  geom_line(
    data = optima_samples,
    mapping = aes(x = x, group = rep_id),
    colour = blues[2],
    stat = "density",
    alpha = 0.5
  ) +
  geom_density(
    data = target_samples,
    mapping = aes(x = x),
    col = highlight_col,
    size = 1.1
  ) +
  scale_x_continuous(limits = c(plot_lower, plot_upper)) +
  # scale_colour_gradientn(
  #   name = expression(log('d'(lambda^{'*'}))),
  #   colours = c(blues[1], blues[3])
  # ) +
  facet_wrap(vars(kappa_fact), nrow = 1, labeller = label_parsed) +
  xlab("Height (cm)") +
  ylab("Density")

ggsave_fullpage(
  filename = args$output,
  plot = p1,
  adjust_height = -14
)
