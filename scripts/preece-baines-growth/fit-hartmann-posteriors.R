library(rstan)
library(fda)
library(dplyr)
library(tidybayes)
library(tidyr)
library(parallel)

source("scripts/common/setup-argparse.R")
source("scripts/common/setup-logger.R")
source("scripts/preece-baines-growth/GLOBALS.R")

parser$add_argument("--hartmann-priors")
parser$add_argument("--pb-model-indiv-hartmann")
args <- parser$parse_args()

# in this one we'll run 5 jobs of 31 cores
# one job for each user_id
task_user_id <- Sys.getenv("SLURM_ARRAY_TASK_ID") %>%
  as.numeric()

hartmann_priors <- readRDS(args$hartmann_priors)
n_hartmann_indiv <- hartmann_priors %>%
  pull(user_id) %>%
  max()

x_mat <- rbind(t(growth$hgtm), t(growth$hgtf))
obs_times <- growth$age
n_indiv <- nrow(x_mat)
n_obs_per_indiv <- ncol(x_mat)

prefit_indiv <- stan_model(args$pb_model_indiv_hartmann)

format_prior_subtbl <- function(prior_subtbl) {
  x <- prior_subtbl %>%
    select(param, lognorm_location, lognorm_scale) %>%
    filter(param != "weib_noise")

  res <- list()
  for (ii in 1 : nrow(x)) {
    vals <- c(x$lognorm_location[ii], x$lognorm_scale[ii])
    vnames <- paste(x$param[ii], colnames(x)[-1], sep = "_")
    names(vals) <- vnames
    res <- c(res, as.list(vals))
  }

  return(res)
}

fit_growth_indiv <- function(indiv_id, prior_subtbl) {
  # do the reshaping of the tbl to the list to stan
  local_stan_data <- c(
    list(
      N_obs_per_indiv = n_obs_per_indiv,
      obs_times = obs_times,
      heights = as.numeric(x_mat[indiv_id, ])
    ),
    format_prior_subtbl(prior_subtbl)
  )

  # standard things
  any_warning <- FALSE
  tryCatch(
    expr = {
      stan_res <<- sampling(
        object = prefit_indiv,
        data = local_stan_data,
        cores = 1,
        chains = 4,
        refresh = 0,
        control = list(
          adapt_delta = 0.95,
          max_treedepth = 12
        )
      )
    },
    warning = function(w) {
      any_warning <<- TRUE
    },
    error = function(e) {
      flog.info(
        "[fda-hartmann] indiv %d: caught error %s",
        indiv_id,
        name = base_filename
      )
    }
  )

  par_samples_long <- stan_res %>%
    gather_draws(h0, delta_h1, s0, delta_s1, theta, noise_sd) %>%
    mutate(indiv_id = indiv_id)

  run_perf_metrics <- tibble(
    min_neff = stan_res %>%
      summary() %>%
      magrittr::extract2("summary") %>%
      magrittr::extract(1 : 6, "n_eff") %>%
      min(),
    max_rhat = stan_res %>%
      summary() %>%
      magrittr::extract2("summary") %>%
      magrittr::extract(1 : 6, "Rhat") %>%
      max(),
    indiv_id = indiv_id
  )

  inner_res <- list(
    any_warning = any_warning,
    run_perf_metrics = run_perf_metrics,
    par_samples = par_samples_long
  )
}

local_prior_tbl <- hartmann_priors %>%
  filter(user_id == task_user_id)

# fit the growth individuals under this prior
all_growth_indiv_res <- lapply(1 : n_indiv, function(indiv_id) {
  flog.info(
    "[fda-hartmann] fitting growth_indiv %d for hartmann_user %d",
    indiv_id,
    task_user_id,
    name = base_filename
  )

  res <- fit_growth_indiv(indiv_id, local_prior_tbl)
  return(res)
})

# collapse the results
hartmann_indiv_res <- list(
  any_warning = lapply(all_growth_indiv_res, function(x) x[["any_warning"]]) %>%
    unlist(),
  run_perf_metrics = all_growth_indiv_res %>%
    extract_and_bind("run_perf_metrics") %>%
    mutate(user_id = task_user_id),
  par_samples = all_growth_indiv_res %>%
    extract_and_bind("par_samples") %>%
    mutate(user_id = task_user_id)
)

saveRDS(
  object = hartmann_indiv_res,
  file = args$output
)
