library(dplyr)
library(patchwork)
library(gridExtra)

source("scripts/common/setup-argparse.R")
source("scripts/common/setup-ggplot-theme.R")

parser$add_argument("--roundtrip-lambda")
parser$add_argument("--output-small")
args <- parser$parse_args()

lambda_tbl <- readRDS(args$roundtrip_lambda)

n_reps_per_target <- 10
priors <- c("normal", "dirichlet_laplace", "regularised_horseshoe")
shape1_vec <- exp(seq(from = log(1 / 3), to  = log(3), length.out = 7))

target_grid_pars <- expand.grid(
  rep_id = 1 : n_reps_per_target,
  prior_type = priors,
  shape1 = shape1_vec,
  shape2 = shape1_vec,
  discrep = "log_ad",
  stringsAsFactors = FALSE
) %>%
  mutate(task_id = 1 : n())

plot_tbl <- lambda_tbl %>%
  left_join(target_grid_pars, by = "task_id") %>%
  mutate(
    facet_factor = sprintf("'Beta' * (%.2f * ',' ~ %.2f)", shape1, shape2),
    param_fact = factor(
      x = parameter,
      levels = c("ig_a1", "ig_b1", "gamma", "a", "p0", "nu", "s_sq"),
      labels = c(
        "italic('a')[1]",
        "italic('b')[1]",
        "gamma",
        "alpha",
        "italic('p')[0]",
        "nu",
        "italic('s')^{2}"
      )
    )
  )

plot_title_strs <- c(
  "normal" = "Gaussian",
  "dirichlet_laplace" = "Dirichlet-Laplace",
  "regularised_horseshoe" = "Regularised Horseshoe"
)

shape_vals <- plot_tbl$shape1 %>%
  unique() %>%
  sort()

small_labels_vals <- shape_vals[c(1, 3, 5, 7)]
small_labels <- plot_tbl %>%
  filter(
    (shape1 %in% small_labels_vals) &
    (shape2 %in% small_labels_vals)
  ) %>%
  pull(facet_factor) %>%
  unique()

small_plot_tbl <- plot_tbl %>%
  filter(facet_factor %in% small_labels)

tiny_plot_tbl <- small_plot_tbl %>%
  filter(shape1 == shape_vals[7])

# plot 1, all cases -- massive plot / 3 plots
plots <- lapply(unique(plot_tbl$prior_type), function(a_prior_type) {
  sub_tbl <- plot_tbl %>%
    filter(prior_type == a_prior_type)

  p <- ggplot(data = sub_tbl, aes(x = optimal_value, y = param_fact)) +
    geom_jitter(width = 0, height = 0.1, alpha = 0.7, pch = 19) +
    facet_wrap(vars(facet_factor) , labeller = label_parsed) +
    scale_y_discrete(labels = function(x) parse(text = x)) +
    ggtitle(label = plot_title_strs[a_prior_type]) +
    xlab("Value at optima") +
    theme(axis.title.y = element_blank())

  return(p)
})

# plot 2, just the cases in the roundtrip talget plot (may have to get smaller)
# than this
small_plots <- lapply(unique(plot_tbl$prior_type), function(a_prior_type) {
  sub_tbl <- small_plot_tbl %>%
    filter(prior_type == a_prior_type)

  p <- ggplot(data = sub_tbl, aes(x = optimal_value, y = param_fact)) +
    geom_jitter(width = 0, height = 0.1, alpha = 0.7, pch = 19) +
    facet_wrap(vars(facet_factor) , labeller = label_parsed) +
    scale_y_discrete(labels = function(x) parse(text = x)) +
    ggtitle(label = plot_title_strs[a_prior_type]) +
    xlab("Value at optima") +
    theme(axis.title.y = element_blank())

  return(p)
})

# tiny plot -- just one row of target tibble
p_tiny <- ggplot(
  data = tiny_plot_tbl,
  aes(x = optimal_value, y = prior_type, colour = prior_type)
) +
  geom_jitter(width = 0, height = 0.1, alpha = 0.7, pch = 19) +
  facet_grid(
    rows = vars(facet_factor),
    cols = vars(param_fact),
    labeller = label_parsed,
    scales = "free_x"
  ) +
  xlab("Value at optimum") +
  theme(
    axis.title.y = element_blank(),
    axis.text.y = element_blank(),
    axis.text.x = element_text(angle = 90),
    strip.text.y = element_text(angle = 270),
    legend.position = "bottom"
  ) +
  scale_colour_manual(
    name = "Prior",
    labels = c(
      dirichlet_laplace = "Dir. Lap.",
      normal = "Gaussian",
      regularised_horseshoe = "Reg. Horse."
    ),
    values = c(
      dirichlet_laplace = highlight_col,
      normal = greens[2],
      regularised_horseshoe = blues[2]
    )
  )

ggsave_fullpage(
  filename = args$output,
  plot = p_tiny,
  adjust_height = -6
)
