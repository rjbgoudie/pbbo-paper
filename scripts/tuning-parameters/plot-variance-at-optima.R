library(ggplot2)

source("scripts/common/setup-ggplot-theme.R")
source("scripts/common/setup-argparse.R")

parser$add_argument("--variance-at-optima-tbl")
args <- parser$parse_args()

plot_tbl <- readRDS(args$variance_at_optima_tbl)

p1 <- ggplot(data = plot_tbl) +
  geom_histogram(mapping = aes(x = x, y = after_stat(density)), bins = 27) +
  facet_grid(rows = vars(prior_samples), cols = vars(importance_samples)) +
  ylab("Density") +
  xlab(expression("log"(italic("D")^{"pop"} * (italic(lambda)))))

ggsave_fullpage(
  plot = p1,
  filename = args$output
)
