#!/bin/bash

#SBATCH -J aam71-batching-and-optimiser-tests
#SBATCH --time=36:00:00
#SBATCH --cpus-per-task=1

## Each task is allocated 5980M (skylake) or 12030M (skylake-himem).
## If this is insufficient, uncomment and edit this line.
## Maximum value 191360M or 384960M.
## #SBATCH --mem=05G

#SBATCH --mail-type=ALL,ARRAY_TASKS
#SBATCH -A mrc-bsu-sl2-cpu
#SBATCH -p skylake

## Array jobs:
#SBATCH --array=1-60

##  - - - - - - - - - - - - - -

## Section 2: Modules

. /etc/profile.d/modules.sh
module purge
module load rhel7/default-peta4

# Load the latest R version.
module load r-4.0.2-gcc-5.4.0-xyx46xb
module load gmake-4.2.1-gcc-5.4.0-anf7tng

CMD="make rds/synthetic-survival/intermediaries/job-$SLURM_ARRAY_TASK_ID.rds"

###############################################################
### You should not have to change anything below this line ####
###############################################################

JOBID=$SLURM_JOB_ID

echo -e "JobID: $JOBID\n======"
echo "Time: `date`"
if [ $SLURM_JOB_NUM_NODES -gt 1 ]; then
        echo "Running on nodes: $SLURM_JOB_NODELIST"
else
        echo "Running on node: `hostname`"
fi

echo "Current directory: `pwd`"
echo -e "\nNum tasks = $SLURM_NTASKS, Num nodes = $SLURM_JOB_NUM_NODES, OMP_NUM_THREADS = $OMP_NUM_THREADS"
echo -e "\nExecuting command:\n==================\n$CMD\n"

eval $CMD