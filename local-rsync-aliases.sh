# TODO: get these names dynamically?

#   --include rds/preece-baines-growth/*.rds \
alias push-latest-code=\
"
rsync \
  -avr \
  -e ssh \
  --update \
  --exclude renv \
  --exclude rmd-reports \
  --exclude package/pbbo/.Rproj.user \
  --exclude .Rproj.user \
  --exclude .git \
  --exclude rds \
  --exclude tex-input \
  . \
  aam71@login.hpc.cam.ac.uk:2021-07-20_priors-by-optimisation
" 

alias pull-latest-results=\
"
rsync \
  -avr \
  -e ssh \
  --update \
  --exclude renv \
  --exclude rmd-reports \
  --exclude package/pbbo/.Rproj.user \
  --exclude .Rproj.user \
  --exclude .git \
  --exclude tex-input \
  --exclude scripts \
  --exclude package \
  aam71@login.hpc.cam.ac.uk:2021-07-20_priors-by-optimisation/ \
  . 
" 

# rsync \
#   -avr \
#   -e ssh \
#   --update \
#   aam71@login.hpc.cam.ac.uk:rds/hpc-work/batching-output \
#   rds/scratch/batching-and-optimiser-tests/
