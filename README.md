# Priors by Bayesian optimisation paper

# Package

The code depends on the [`pbbo` package](http://github.com/hhau/pbbo), which will need to be installed off github.
The code depends on the [`pbbo` package](http://github.com/hhau/pbbo), which will need to be installed off GitHub.

# Notes on rerunning this code

- Much of the code is written assuming one is using a SLURM-based HPC environment.
- The jobs are stored in `slurm/job*.txt`
  - The only dependency between the jobs is that `job-pb-optima-posteriors.txt` depends on both `job-pb-{{pop | cov}}-prior.txt` and the corresponding `process-{{population | covariate}}-runs.R` scripts, so those must be run successfully first.
- Some of the posterior results, obtained from `scripts/preece-baines-growth/fit-fda-{{ flat | optima }}-prior.R` occasionally fail (due to what I only imagine are Stan gremlins or tempfile issues). Rerunning the failing jobs a second time seems to cure this.- Steps in between the slurm jobs are controlled by `GNU Make`.
- The code assumes all folders and `intermediaries` subfolders exist. Hopefully I have left a `.gitkeep` in all the necessary folder, but I may have forgotten some.

## Detailed steps for running each of the examples

For each of these examples, you will need to

- Recreate the `R` environment using `renv::restore`. This should also, but may not, install the [`pbbo` package](http://github.com/hhau/pbbo) which may need manually installing using `remotes::install_github('hhau/pbbo')`.
- Have `GNU Make 4.3` installed (the latest version is required, I use `&:`). 
- Have access to a HPC environment that uses `slurm`, and that all the commands in the `slurm/job-*.txt` are appropriate for this environment.
- Adjust the number of jobs in the call to make to something appropriate.

### PB / Human height example

1. `sbatch slurm/job-pb-pop-prior.txt` and `sbatch slurm/job-pb-cov-prior.txt`
2. (optional) move results to analysis environment.
3.  `make -j 2 rds/preece-baines-growth/population-optima-results-long.rds rds/preece-baines-growth/covariate-optima-results-long.rds`
4. (semi-optional) If you moved the results to a different location for analysis, move the contents of `rds/preece-baines-growth/*.rds` back to the HPC environment.
5.  `make -j 5  plots/preece-baines-growth/pop-kappa.pdf plots/preece-baines-growth/cov-kappa.pdf plots/preece-baines-growth/population-target-comparsion.pdf plots/preece-baines-growth/covariate-target-comparsion.pdf plots/preece-baines-growth/both-optima-discrep-hists.pdf plots/preece-baines-growth/fda-flat-prior-matrix-plot.png plots/preece-baines-growth/fda-all-any-warnings-plot.png plots/preece-baines-growth/regression-prior-preds.pdf plots/preece-baines-growth/regression-post-preds.pdf plots/preece-baines-growth/pop-priors-posteriors-compare.png plots/preece-baines-growth/cov-priors-posteriors-compare.png plots/preece-baines-growth/small-cov-priors-posteriors.pdf`

### R² example

1. `sbatch slurm/job-r2-asymp.txt` and `sbatch slurm/job-r2-roundtrip.txt`
2. (optional) move the output using `rysnc` to an appropriate analysis environment.
3. `make -j 2 plots/r2-example/gamma-diff-plot.pdf plots/r2-example/optimal-pf-draws-plot.pdf plots/r2-example/normal-noise-hyperpars-plot.pdf plots/r2-example/roundtrip-target-plot-big.pdf plots/r2-example/roundtrip-target-plot-small.pdf plots/r2-example/roundtrip-target-lambda-tiny.pdf` 

### Cure fraction survival example

1. `sbatch slurm/job-surv-example.txt`
2. (optional) move results to analysis environment.
3. `make -j 4 plots/synthetic-survival/all-theta-over-kappa.png plots/synthetic-survival/all-pareto-fronts-by-kappa.pdf plots/synthetic-survival/subset-indivs-ppd-y-dens.png plots/synthetic-survival/beta-covariance-pair.pdf`
