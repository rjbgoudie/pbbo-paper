RSCRIPT = Rscript
PLOT_SETTINGS = scripts/common/setup-ggplot-theme.R
TEX_FILES = $(wildcard tex-input/*.tex) \
	$(wildcard tex-input/*/*.tex) \
	$(wildcard tex-input/*/*/*.tex)

# useful compound make components
PLOTS = plots
RDS = rds
SCRIPTS = scripts
MODELS = $(SCRIPTS)/models

# if you wildcard the all-target, then nothing will happen if the target doesn't
# exist (no target). hard code the target.
# CHANGE THIS:
BASENAME = priors-by-optimisation
WRITEUP = $(BASENAME).pdf

all : $(WRITEUP)

clean : 
	trash $(BASENAME).aux $(BASENAME).out $(BASENAME).pdf $(BASENAME).tex \
	slurm-*.out

# ====================== Synthetic data example/test ===============
SYN_BASENAME = synthetic-survival
SYN_RDS = $(RDS)/$(SYN_BASENAME)
SYN_SCRIPTS = $(SCRIPTS)/$(SYN_BASENAME)
SYN_PLOTS = $(PLOTS)/$(SYN_BASENAME)
SYN_GLOBALS = $(SYN_SCRIPTS)/GLOBALS.R

SYN_PROCESSING_FUNCS = $(SYN_SCRIPTS)/processing-functions.R

## outputs
SYN_INITIAL_DATA = $(SYN_RDS)/censoring-and-covariate-data.rds

SYN_INTERMEDIARIES = $(SYN_RDS)/intermediaries
SYN_ALL_JOBS = $(SYN_INTERMEDIARIES)/$(wildcard job*.rds)

SYN_PPD_Y_SAMPLES = $(SYN_RDS)/full-prior-pred-y-samples.rds
SYN_PPD_THETA_SAMPLES = $(SYN_RDS)/full-prior-theta-samples.rds
SYN_PPD_Y_DENS = $(SYN_RDS)/full-prior-pred-y-dens-by-indiv.rds
SYN_PARETO_FRONTS = $(SYN_RDS)/full-pareto-fronts-by-kappa.rds
SYN_COVARIANCE_PAIRS = $(SYN_RDS)/covariance-pairs-tbl.rds

## plots
SYN_THETA_PLOT = $(SYN_PLOTS)/all-theta-over-kappa.png
SYN_PARETO_FRONTS_PLOT = $(SYN_PLOTS)/all-pareto-fronts-by-kappa.pdf
SYN_PPD_Y_DENS_PLOT = $(SYN_PLOTS)/subset-indivs-ppd-y-dens.png
SYN_PAIRS_PLOT = $(SYN_PLOTS)/beta-covariance-pair.pdf

ALL_PLOTS = $(SYN_THETA_PLOT) $(SYN_PARETO_FRONTS_PLOT) $(SYN_PPD_Y_DENS_PLOT)

## rules
$(SYN_INITIAL_DATA) : \
	$(SYN_SCRIPTS)/simulate-synthetic-data.R \
	$(SYN_GLOBALS)
	$(RSCRIPT) $< \
		--output $(SYN_INITIAL_DATA)

SYN_TASK_TARGET = $(SYN_INTERMEDIARIES)/job-$(SLURM_ARRAY_TASK_ID).rds
$(SYN_TASK_TARGET) : \
	$(SYN_SCRIPTS)/run-surv-example.R \
	$(SYN_GLOBALS) \
	$(SYN_INITIAL_DATA)
	$(RSCRIPT) $< \
		--censoring-and-covariate-data $(SYN_INITIAL_DATA) \
		--output $@

$(SYN_PPD_Y_SAMPLES) \
$(SYN_PPD_THETA_SAMPLES) \
$(SYN_PPD_Y_DENS) \
$(SYN_PARETO_FRONTS) &: \
	$(SYN_SCRIPTS)/process-surv-output.R \
	$(SYN_GLOBALS) \
	$(SYN_PROCESSING_FUNCS) \
	$(SYN_INITIAL_DATA) \
	$(SYN_ALL_JOBS)
	$(RSCRIPT) $< \
		--censoring-and-covariate-data $(SYN_INITIAL_DATA) \
		--output-ppd-y-samples $(SYN_PPD_Y_SAMPLES) \
		--output-ppd-theta-samples $(SYN_PPD_THETA_SAMPLES) \
		--output-pareto-fronts $(SYN_PARETO_FRONTS) \
		--output $(SYN_PPD_Y_DENS)

$(SYN_COVARIANCE_PAIRS) : \
	$(SYN_SCRIPTS)/process-for-covariance-structure.R \
	$(SYN_GLOBALS) \
	$(SYN_ALL_JOBS)
	$(RSCRIPT) $< \
		--intermediaries-dir $(SYN_INTERMEDIARIES) \
		--output $@

$(SYN_THETA_PLOT) : \
	$(SYN_SCRIPTS)/plot-theta.R \
	$(SYN_PPD_THETA_SAMPLES)
	$(RSCRIPT) $< \
		--ppd-theta-samples $(SYN_PPD_THETA_SAMPLES) \
		--output $@

$(SYN_PARETO_FRONTS_PLOT) : \
	$(SYN_SCRIPTS)/plot-pareto-fronts.R \
	$(SYN_PARETO_FRONTS)
	$(RSCRIPT) $< \
		--pareto-fronts $(SYN_PARETO_FRONTS) \
		--output $@

$(SYN_PPD_Y_DENS_PLOT) : \
	$(SYN_SCRIPTS)/plot-ppds.R \
	$(SYN_PPD_Y_DENS)
	$(RSCRIPT) $< \
		--ppd-y-dens $(SYN_PPD_Y_DENS) \
		--output $@

$(SYN_PAIRS_PLOT) : \
	$(SYN_SCRIPTS)/plot-beta-pairs.R \
	$(SYN_COVARIANCE_PAIRS)
	$(RSCRIPT) $< \
		--covariance-pairs-plot-tbl $(SYN_COVARIANCE_PAIRS) \
		--output $@

ALL_PLOTS += $(SYN_PAIRS_PLOT)

# ================= R2 example =========================
R2_BASENAME = r2-examples
R2_RDS = $(RDS)/$(R2_BASENAME)
R2_SCRIPTS = $(SCRIPTS)/$(R2_BASENAME)
R2_PLOTS = $(PLOTS)/$(R2_BASENAME)
R2_GLOBALS = $(R2_SCRIPTS)/GLOBALS.R

## partials
R2_ASYMP_FUNCTIONS = $(R2_SCRIPTS)/asymp-functions.R
R2_ASYMP_INTERMEDIARIES = $(R2_RDS)/asymp-intermediaries

R2_ROUNDTRIP_FUNCTIONS = $(R2_SCRIPTS)/roundtrip-functions.R
R2_ROUNDTRIP_INTERMEDIARES = $(R2_RDS)/roundtrip-intermediaries

## outputs
R2_ALL_ASYMP_ROWS = $(R2_ASYMP_INTERMEDIARIES)/$(wildcard row-*.rds)
R2_ASYMP_NORMAL_RESULTS = $(R2_RDS)/asymp-r2-simstudy-output.rds

R2_ALL_ROUNDTRIP_ROWS = $(R2_ROUNDTRIP_INTERMEDIARES)/$(wildcard row-*.rds)
R2_ROUNDTRIP_RESULTS_PROCESSED = $(R2_RDS)/roundtrip-results-processed.rds
R2_ROUNDTRIP_RESULTS_LAMBDA = $(R2_RDS)/roundtrip-results-lambda.rds

R2_GAMMA_DIFF_PLOT = $(R2_PLOTS)/gamma-diff-plot.pdf
R2_OPTIMAL_PF_DRAWS_PLOT = $(R2_PLOTS)/optimal-pf-draws-plot.pdf
R2_NORMAL_NOISE_HYPERPARS_PLOT = $(R2_PLOTS)/normal-noise-hyperpars-plot.pdf

R2_ROUNDTRIP_PLOT_BIG = $(R2_PLOTS)/roundtrip-target-plot-big.pdf
R2_ROUNDTRIP_PLOT_SMALL = $(R2_PLOTS)/roundtrip-target-plot-small.pdf
R2_ROUNDTRIP_PLOT_LAMBDA_TINY = $(R2_PLOTS)/roundtrip-target-lambda-tiny.pdf

## rules
## asmyp array job
R2_ASYMP_TASK_TARGET = $(R2_ASYMP_INTERMEDIARIES)/row-$(SLURM_ARRAY_TASK_ID).rds
$(R2_ASYMP_TASK_TARGET) : \
	$(R2_SCRIPTS)/run-asymp.R \
	$(R2_ASYMP_FUNCTIONS)
	$(RSCRIPT) $< \
		--asymp-functions $(R2_ASYMP_FUNCTIONS) \
		--asymp-intermedaries-dir $(R2_ASYMP_INTERMEDIARIES) \
		--output $@

$(R2_ASYMP_NORMAL_RESULTS) : \
	$(R2_SCRIPTS)/process-asymp.R \
	$(R2_ASYMP_FUNCTIONS) \
	$(R2_ALL_ASYMP_ROWS)
	$(RSCRIPT) $< \
		--asymp-intermediaries-dir $(R2_ASYMP_INTERMEDIARIES) \
		--asymp-functions $(R2_ASYMP_FUNCTIONS) \
		--output $@

$(R2_GAMMA_DIFF_PLOT) \
$(R2_OPTIMAL_PF_DRAWS_PLOT) \
$(R2_NORMAL_NOISE_HYPERPARS_PLOT) &: \
	$(R2_SCRIPTS)/plot-asymp.R \
	$(R2_ASYMP_NORMAL_RESULTS)
	$(RSCRIPT) $< \
		--r2-asymp-normal-results $(R2_ASYMP_NORMAL_RESULTS) \
		--output-optimal-pf-draws-plot $(R2_OPTIMAL_PF_DRAWS_PLOT) \
		--normal-noise-hyperpars-plot $(R2_NORMAL_NOISE_HYPERPARS_PLOT) \
		--output $(R2_GAMMA_DIFF_PLOT)

ALL_PLOTS += $(R2_GAMMA_DIFF_PLOT) $(R2_OPTIMAL_PF_DRAWS_PLOT) $(R2_NORMAL_NOISE_HYPERPARS_PLOT)

## Roundtrip array job
R2_LOCAL_TASK_TARGET = $(R2_ROUNDTRIP_INTERMEDIARES)/row-$(SLURM_ARRAY_TASK_ID).rds

$(R2_LOCAL_TASK_TARGET) : \
	$(R2_SCRIPTS)/run-roundtrip.R \
	$(R2_ROUNDTRIP_FUNCTIONS)
	$(RSCRIPT) $< \
		--roundtrip-functions $(R2_ROUNDTRIP_FUNCTIONS) \
		--output $@

$(R2_ROUNDTRIP_RESULTS_PROCESSED) \
$(R2_ROUNDTRIP_RESULTS_LAMBDA) &: \
	$(R2_SCRIPTS)/process-roundtrip.R \
	$(R2_ALL_ROUNDTRIP_ROWS)
	$(RSCRIPT) $< \
		--intermediaries-dir $(R2_ROUNDTRIP_INTERMEDIARES) \
		--output-lambda-optima $(R2_ROUNDTRIP_RESULTS_LAMBDA) \
		--output $@

$(R2_ROUNDTRIP_PLOT_BIG) \
$(R2_ROUNDTRIP_PLOT_SMALL) &: \
	$(R2_SCRIPTS)/plot-roundtrip.R \
	$(R2_ROUNDTRIP_RESULTS_PROCESSED)
	$(RSCRIPT) $< \
		--roundtrip-results $(R2_ROUNDTRIP_RESULTS_PROCESSED) \
		--output-small $(R2_ROUNDTRIP_PLOT_SMALL) \
		--output $(R2_ROUNDTRIP_PLOT_BIG)

ALL_PLOTS += $(R2_ROUNDTRIP_PLOT_BIG) $(R2_ROUNDTRIP_PLOT_SMALL)

$(R2_ROUNDTRIP_PLOT_LAMBDA_TINY) : \
	$(R2_SCRIPTS)/plot-roundtrip-lambda.R \
	$(R2_ROUNDTRIP_RESULTS_LAMBDA)
	$(RSCRIPT) $< \
		--roundtrip-lambda $(R2_ROUNDTRIP_RESULTS_LAMBDA) \
		--output $@

ALL_PLOTS += $(R2_ROUNDTRIP_PLOT_LAMBDA_TINY)

# ================== Preece-Baines (PB) example ====================
PB_BASENAME = preece-baines-growth
PB_RDS = $(RDS)/$(PB_BASENAME)
PB_SCRIPTS = $(SCRIPTS)/$(PB_BASENAME)
PB_PLOTS = $(PLOTS)/$(PB_BASENAME)
PB_GLOBALS = $(PB_SCRIPTS)/GLOBALS.R

## partials
PB_POPULATION_INTERMEDIARES = $(PB_RDS)/population-intermediaries
PB_COVARIATE_INTERMEDIARES = $(PB_RDS)/covariate-intermediaries
PB_FDA_FLAT_INTERMEDIARES = $(PB_RDS)/fit-fda-flat-intermediaries
PB_FDA_POPULATION_OPTIMA_INTERMEDIARES = $(PB_RDS)/fit-fda-pop-opt-intermediaries
PB_FDA_COVARIATE_OPTIMA_INTERMEDIARES = $(PB_RDS)/fit-fda-cov-opt-intermediaries
PB_FDA_HARTMANN_INTERMEDIARES = $(PB_RDS)/fit-fda-hartmann-intermediaries

## models
PB_INDIV_STAN_MODEL = $(MODELS)/preece-baines-indiv.stan
PB_INDIV_OUR_PARAM_STAN_MODEL = $(MODELS)/preece-baines-indiv-our-parameterisation.stan
PB_INDIV_HARTMANN_MODEL = $(MODELS)/preece-baines-indiv-hartmann.stan

## outputs
## RDS
PB_POPULATION_ALL_ROWS = $(PB_POPULATION_INTERMEDIARES)/$(wildcard row-*.rds)
PB_COVARIATE_ALL_ROWS = $(PB_COVARIATE_INTERMEDIARES)/$(wildcard row-*.rds)

## need to pick best kappa before we do the rest of the processing
PB_POP_BEST_KAPPA = $(PB_RDS)/pop-best-kappa.rds
PB_COV_BEST_KAPPA = $(PB_RDS)/cov-best-kappa.rds

## these results are for the prior optimisation process -- not posterior samples
PB_POPULATION_FULL_RESULTS_LONG = $(PB_RDS)/population-full-results-long.rds
PB_POPULATION_FULL_RESULTS_WIDE = $(PB_RDS)/population-full-results-wide.rds
PB_POPULATION_OPTIMA_RESULTS_LONG = $(PB_RDS)/population-optima-results-long.rds

PB_COVARIATE_FULL_RESULTS_LONG = $(PB_RDS)/covariate-full-results-long.rds
PB_COVARIATE_FULL_RESULTS_WIDE = $(PB_RDS)/covariate-full-results-wide.rds
PB_COVARIATE_OPTIMA_RESULTS_LONG = $(PB_RDS)/covariate-optima-results-long.rds

## these are more posterior related
PB_FDA_FLAT_ALL_RUNS = $(PB_FDA_FLAT_INTERMEDIARES)/$(wildcard run-*.rds)
PB_FDA_POP_ALL_RUNS = $(PB_FDA_POPULATION_OPTIMA_INTERMEDIARES)/$(wildcard run-*.rds)
PB_FDA_COV_ALL_RUNS = $(PB_FDA_COVARIATE_OPTIMA_INTERMEDIARES)/$(wildcard run-*.rds)
PB_FDA_HARTMANN_ALL_RUNS = $(PB_FDA_HARTMANN_INTERMEDIARES)/$(wildcard user-*.rds)

PB_FDA_FLAT_ANY_WARNINGS  = $(PB_RDS)/fda-flat-any-warnings.rds
#PB_FDA_FLAT_PERFORMANCE_METRICS = $(PB_RDS)/fda-flat-performance-metrics.rds
#PB_FDA_FLAT_POSTERIOR_SAMPLES = $(PB_RDS)/fda-flat-posterior-samples.rds

PB_FDA_POP_ANY_WARNINGS  = $(PB_RDS)/fda-optima-pop-any-warnings.rds
PB_FDA_POP_PERFORMANCE_METRICS = $(PB_RDS)/fda-optima-pop-performance-metrics.rds
PB_FDA_POP_POSTERIOR_SAMPLES = $(PB_RDS)/fda-optima-pop-posterior-samples.rds
PB_FDA_COV_ANY_WARNINGS = $(PB_RDS)/fda-optima-cov-any-warnings.rds
PB_FDA_COV_PERFORMANCE_METRICS = $(PB_RDS)/fda-optima-cov-performance-metrics.rds
PB_FDA_COV_POSTERIOR_SAMPLES = $(PB_RDS)/fda-optima-cov-posterior-samples.rds

PB_HARTMANN_PRIORS = $(PB_RDS)/hartmann-priors.rds

PB_PRIOR_PRED_DATA = $(PB_RDS)/regression-prior-quantiles.rds
PB_POST_PRED_DATA = $(PB_RDS)/regression-post-quantiles.rds

PB_ALL_PRIORS_AND_POSTERIORS_COMPARE = $(PB_RDS)/all-priors-posteriors-compare.rds

## Plots
PB_POP_KAPPA_PLOT = $(PB_PLOTS)/pop-kappa.pdf
PB_COV_KAPPA_PLOT = $(PB_PLOTS)/cov-kappa.pdf
ALL_PLOTS += $(PB_POP_KAPPA_PLOT) $(PB_COV_KAPPA_PLOT)

PB_POPULATION_TARGET_COMPARISON_PLOT = $(PB_PLOTS)/population-target-comparsion.pdf
PB_COVARIATE_TARGET_COMPARISON_PLOT = $(PB_PLOTS)/covariate-target-comparsion.pdf
ALL_PLOTS += $(PB_POPULATION_TARGET_COMPARISON_PLOT) $(PB_COVARIATE_TARGET_COMPARISON_PLOT)

PB_BOTH_OPTIMA_DISCREP_HISTS = $(PB_PLOTS)/both-optima-discrep-hists.pdf
ALL_PLOTS += $(PB_BOTH_OPTIMA_DISCREP_HISTS)

PB_FDA_FLAT_MATPLOT = $(PB_PLOTS)/fda-flat-prior-matrix-plot.png
ALL_PLOTS += $(PB_FDA_FLAT_MATPLOT)

PB_FDA_ALL_ANY_WARNINGS_PLOT = $(PB_PLOTS)/fda-all-any-warnings-plot.png
ALL_PLOTS += $(PB_FDA_ALL_ANY_WARNINGS_PLOT)

PB_PRIOR_PRED_PLOT = $(PB_PLOTS)/regression-prior-preds.pdf
PB_FDA_WOSRT_INDIV_POST_PRED_PLOT = $(PB_PLOTS)/regression-post-preds.pdf
ALL_PLOTS += $(PB_PRIOR_PRED_PLOT) $(PB_FDA_WOSRT_INDIV_POST_PRED_PLOT)

PB_POP_PRIORS_POSTERIORS_PLOT = $(PB_PLOTS)/pop-priors-posteriors-compare.png
PB_COV_PRIORS_POSTERIORS_PLOT = $(PB_PLOTS)/cov-priors-posteriors-compare.png
PB_SMALL_COV_PRIORS_POSTERIORS_PLOT = $(PB_PLOTS)/small-cov-priors-posteriors.pdf
ALL_PLOTS += $(PB_POP_PRIORS_POSTERIORS_PLOT) $(PB_COV_PRIORS_POSTERIORS_PLOT) $(PB_SMALL_COV_PRIORS_POSTERIORS_PLOT)

## rules
## population marginal example
PB_POPULATION_TASK_TARGET = $(PB_POPULATION_INTERMEDIARES)/row-$(SLURM_ARRAY_TASK_ID).rds

$(PB_POPULATION_TASK_TARGET) : \
	$(PB_SCRIPTS)/run-example-population.R \
	$(PB_GLOBALS)
	$(RSCRIPT) $< \
		--output $@

$(PB_POP_BEST_KAPPA) \
$(PB_POP_KAPPA_PLOT) &: \
	$(PB_SCRIPTS)/find-plot-kappa-pop.R \
	$(PB_GLOBALS) \
	$(PB_POPULATION_ALL_ROWS)
	$(RSCRIPT) $< \
		--population-intermediaries-dir $(PB_POPULATION_INTERMEDIARES) \
		--output-pop-best-kappa $(PB_POP_BEST_KAPPA) \
		--output $(PB_POP_KAPPA_PLOT)

$(PB_POPULATION_FULL_RESULTS_LONG) \
$(PB_POPULATION_FULL_RESULTS_WIDE) \
$(PB_POPULATION_OPTIMA_RESULTS_LONG) &: \
	$(PB_SCRIPTS)/process-population-runs.R \
	$(PB_POPULATION_ALL_ROWS) \
	$(PB_POP_BEST_KAPPA)
	$(RSCRIPT) $< \
		--population-intermediaries-dir $(PB_POPULATION_INTERMEDIARES) \
		--pop-best-kappa $(PB_POP_BEST_KAPPA) \
		--output-optima-long $(PB_POPULATION_OPTIMA_RESULTS_LONG) \
		--output-full-wide $(PB_POPULATION_FULL_RESULTS_WIDE) \
		--output $(PB_POPULATION_FULL_RESULTS_LONG)

$(PB_POPULATION_TARGET_COMPARISON_PLOT) : \
	$(PB_SCRIPTS)/plot-population-target-comparison.R \
	$(PB_POPULATION_OPTIMA_RESULTS_LONG)
	$(RSCRIPT) $< \
		--optima-results-long $(PB_POPULATION_OPTIMA_RESULTS_LONG) \
		--output $@

## Covariate example
PB_COVARIATE_TASK_TARGET = $(PB_COVARIATE_INTERMEDIARES)/row-$(SLURM_ARRAY_TASK_ID).rds

$(PB_COVARIATE_TASK_TARGET) : \
	$(PB_SCRIPTS)/run-example-covariates.R \
	$(PB_GLOBALS)
	$(RSCRIPT) $< \
		--output $@

$(PB_COV_BEST_KAPPA) \
$(PB_COV_KAPPA_PLOT) &: \
	$(PB_SCRIPTS)/find-plot-kappa-cov.R \
	$(PB_GLOBALS) \
	$(PB_COVARIATE_ALL_ROWS)
	$(RSCRIPT) $< \
		--covariate-intermediaries-dir $(PB_COVARIATE_INTERMEDIARES) \
		--output-cov-best-kappa $(PB_COV_BEST_KAPPA) \
		--output $(PB_COV_KAPPA_PLOT)

$(PB_COVARIATE_FULL_RESULTS_LONG) \
$(PB_COVARIATE_FULL_RESULTS_WIDE) \
$(PB_COVARIATE_OPTIMA_RESULTS_LONG) &: \
	$(PB_SCRIPTS)/process-covariate-runs.R \
	$(PB_COVARIATE_ALL_ROWS) \
	$(PB_COV_BEST_KAPPA)
	$(RSCRIPT) $< \
		--covariate-intermediaries-dir $(PB_COVARIATE_INTERMEDIARES) \
		--cov-best-kappa $(PB_COV_BEST_KAPPA) \
		--output-optima-long $(PB_COVARIATE_OPTIMA_RESULTS_LONG) \
		--output-full-wide $(PB_COVARIATE_FULL_RESULTS_WIDE) \
		--output $(PB_COVARIATE_FULL_RESULTS_LONG)

$(PB_COVARIATE_TARGET_COMPARISON_PLOT) : \
	$(PB_SCRIPTS)/plot-covariate-target-comparison.R \
	$(PB_COVARIATE_OPTIMA_RESULTS_LONG)
	$(RSCRIPT) $< \
		--optima-results-long $(PB_COVARIATE_OPTIMA_RESULTS_LONG) \
		--output $@

## both priors but no posteriors
$(PB_BOTH_OPTIMA_DISCREP_HISTS) : \
	$(PB_SCRIPTS)/plot-optima-discrep-hists.R \
	$(PB_POPULATION_OPTIMA_RESULTS_LONG) \
	$(PB_COVARIATE_OPTIMA_RESULTS_LONG)
	$(RSCRIPT) $< \
		--pop-optima-long $(PB_POPULATION_OPTIMA_RESULTS_LONG) \
		--cov-optima-long $(PB_COVARIATE_OPTIMA_RESULTS_LONG) \
		--output $@

## Posterior comparison example
PB_FDA_FLAT_TASK_TARGET = $(PB_FDA_FLAT_INTERMEDIARES)/run-$(SLURM_ARRAY_TASK_ID).rds
$(PB_FDA_FLAT_TASK_TARGET) : \
	$(PB_SCRIPTS)/fit-fda-flat-prior.R
	$(RSCRIPT) $< \
		--pb-model-indiv $(PB_INDIV_STAN_MODEL) \
		--output $@

$(PB_FDA_FLAT_ANY_WARNINGS) \
$(PB_FDA_FLAT_MATPLOT) &: \
	$(PB_SCRIPTS)/plot-fda-flat-prior-warnings.R \
	$(PB_FDA_FLAT_ALL_RUNS)
	$(RSCRIPT) $< \
		--fda-flat-intermediaries $(PB_FDA_FLAT_INTERMEDIARES) \
		--output-flat-warnings $(PB_FDA_FLAT_ANY_WARNINGS) \
		--output $(PB_FDA_FLAT_MATPLOT)

PB_FDA_POPULATIION_OPTIMA_TASK_TARGET = $(PB_FDA_POPULATION_OPTIMA_INTERMEDIARES)/run-$(SLURM_ARRAY_TASK_ID).rds
PB_FDA_COVARIATE_OPTIMA_TASK_TARGET = $(PB_FDA_COVARIATE_OPTIMA_INTERMEDIARES)/run-$(SLURM_ARRAY_TASK_ID).rds

$(PB_FDA_COVARIATE_OPTIMA_TASK_TARGET) \
$(PB_FDA_POPULATIION_OPTIMA_TASK_TARGET) &: \
	$(PB_SCRIPTS)/fit-fda-optima-priors.R \
	$(PB_COVARIATE_OPTIMA_RESULTS_LONG) \
	$(PB_POPULATION_OPTIMA_RESULTS_LONG) \
	$(PB_INDIV_OUR_PARAM_STAN_MODEL)
	$(RSCRIPT) $< \
		--covariate-optima-results-long $(PB_COVARIATE_OPTIMA_RESULTS_LONG) \
		--population-optima-results-long $(PB_POPULATION_OPTIMA_RESULTS_LONG) \
		--pb-model-indiv-optima $(PB_INDIV_OUR_PARAM_STAN_MODEL) \
		--output-population $(PB_FDA_POPULATIION_OPTIMA_TASK_TARGET) \
		--output $(PB_FDA_COVARIATE_OPTIMA_TASK_TARGET)

$(PB_FDA_POP_ANY_WARNINGS) \
$(PB_FDA_POP_PERFORMANCE_METRICS) \
$(PB_FDA_POP_POSTERIOR_SAMPLES) \
$(PB_FDA_COV_ANY_WARNINGS) \
$(PB_FDA_COV_PERFORMANCE_METRICS) \
$(PB_FDA_COV_POSTERIOR_SAMPLES) &: \
	$(PB_SCRIPTS)/process-fda-optima-prior.R \
	$(PB_FDA_POP_ALL_RUNS) \
	$(PB_FDA_COV_ALL_RUNS)
	$(RSCRIPT) $< \
		--fda-pop-optima-intermediaries $(PB_FDA_POPULATION_OPTIMA_INTERMEDIARES) \
		--fda-cov-optima-intermediaries $(PB_FDA_COVARIATE_OPTIMA_INTERMEDIARES) \
		--fda-pop-any-warnings $(PB_FDA_POP_ANY_WARNINGS) \
		--fda-pop-performance-metrics $(PB_FDA_POP_PERFORMANCE_METRICS) \
		--fda-pop-posterior-samples $(PB_FDA_POP_POSTERIOR_SAMPLES) \
		--fda-cov-any-warnings $(PB_FDA_COV_ANY_WARNINGS) \
		--fda-cov-performance-metrics $(PB_FDA_COV_PERFORMANCE_METRICS) \
		--fda-cov-posterior-samples $(PB_FDA_COV_POSTERIOR_SAMPLES) \
		--output ""

$(PB_FDA_ALL_ANY_WARNINGS_PLOT) : \
	$(PB_SCRIPTS)/plot-fda-all-prior-warnings.R \
	$(PB_FDA_POP_ANY_WARNINGS) \
	$(PB_FDA_COV_ANY_WARNINGS) \
	$(PB_FDA_FLAT_ALL_RUNS) \
	$(PB_FDA_HARTMANN_ALL_RUNS)
	$(RSCRIPT) $< \
		--fda-hartmann-intermediaries $(PB_FDA_HARTMANN_INTERMEDIARES) \
		--fda-flat-intermediaries $(PB_FDA_FLAT_INTERMEDIARES) \
		--fda-pop-warnings $(PB_FDA_POP_ANY_WARNINGS) \
		--fda-cov-warnings $(PB_FDA_COV_ANY_WARNINGS) \
		--output $@

## prior/post predictive comparison on the regression scale
$(PB_PRIOR_PRED_DATA) : \
	$(PB_SCRIPTS)/prepare-prior-pred-regression.R \
	$(PB_POPULATION_OPTIMA_RESULTS_LONG) \
	$(PB_COVARIATE_OPTIMA_RESULTS_LONG) \
	$(PB_HARTMANN_PRIORS)
	$(RSCRIPT) $< \
		--pop-prior-optima-long $(PB_POPULATION_OPTIMA_RESULTS_LONG) \
		--cov-prior-optima-long $(PB_COVARIATE_OPTIMA_RESULTS_LONG) \
		--hartmann-priors $(PB_HARTMANN_PRIORS) \
		--output $@

$(PB_POST_PRED_DATA) : \
	$(PB_SCRIPTS)/prepare-posterior-pred-regression.R \
	$(PB_FDA_FLAT_ANY_WARNINGS) \
	$(PB_FDA_POP_POSTERIOR_SAMPLES) \
	$(PB_FDA_COV_POSTERIOR_SAMPLES) \
	$(PB_FDA_FLAT_ALL_RUNS) \
	$(PB_FDA_HARTMANN_ALL_RUNS)
	$(RSCRIPT) $< \
		--fda-flat-warnings $(PB_FDA_FLAT_ANY_WARNINGS) \
		--fda-pop-opt-samples $(PB_FDA_POP_POSTERIOR_SAMPLES) \
		--fda-cov-opt-samples $(PB_FDA_COV_POSTERIOR_SAMPLES) \
		--fda-flat-intermediaries-dir $(PB_FDA_FLAT_INTERMEDIARES) \
		--fda-hartmann-intermediaries-dir $(PB_FDA_HARTMANN_INTERMEDIARES) \
		--output $@

$(PB_PRIOR_PRED_PLOT) : \
	$(PB_SCRIPTS)/plot-regression-prior.R \
	$(PB_PRIOR_PRED_DATA)
	$(RSCRIPT) $< \
		--prior-pred-plot-data $(PB_PRIOR_PRED_DATA) \
		--output $@

$(PB_FDA_WOSRT_INDIV_POST_PRED_PLOT) : \
	$(PB_SCRIPTS)/plot-regression-posterior.R \
	$(PB_POST_PRED_DATA)
	$(RSCRIPT) $< \
		--post-pred-plot-data $(PB_POST_PRED_DATA) \
		--output $@

## Comparisons to Hartmann
$(PB_HARTMANN_PRIORS) : \
	$(PB_SCRIPTS)/derive-hartmann-pooled-prior.R
	$(RSCRIPT) $< \
		--output $@

PB_FDA_HARTMANN_TASK_TARGET = $(PB_FDA_HARTMANN_INTERMEDIARES)/user-$(SLURM_ARRAY_TASK_ID).rds

$(PB_FDA_HARTMANN_TASK_TARGET) : \
	$(PB_SCRIPTS)/fit-hartmann-posteriors.R \
	$(PB_HARTMANN_PRIORS) \
	$(PB_INDIV_HARTMANN_MODEL)
	$(RSCRIPT) $< \
		--hartmann-priors $(PB_HARTMANN_PRIORS) \
		--pb-model-indiv-hartmann $(PB_INDIV_HARTMANN_MODEL) \
		--output $@

$(PB_ALL_PRIORS_AND_POSTERIORS_COMPARE) : \
	$(PB_SCRIPTS)/process-all-priors-posteriors-compare.R \
	$(PB_POPULATION_OPTIMA_RESULTS_LONG) \
	$(PB_COVARIATE_OPTIMA_RESULTS_LONG) \
	$(PB_FDA_POP_POSTERIOR_SAMPLES) \
	$(PB_FDA_COV_POSTERIOR_SAMPLES) \
	$(PB_FDA_FLAT_ANY_WARNINGS) \
	$(PB_FDA_FLAT_INTERMEDIARES) \
	$(PB_HARTMANN_PRIORS) \
	$(PB_FDA_HARTMANN_ALL_RUNS)
	$(RSCRIPT) $< \
		--prior-population-optima-results $(PB_POPULATION_OPTIMA_RESULTS_LONG) \
		--prior-covariate-optima-results $(PB_COVARIATE_OPTIMA_RESULTS_LONG) \
		--post-pop-optima-samples $(PB_FDA_POP_POSTERIOR_SAMPLES) \
		--post-cov-optima-samples $(PB_FDA_COV_POSTERIOR_SAMPLES) \
		--post-flat-any-warnings $(PB_FDA_FLAT_ANY_WARNINGS) \
		--flat-intermediaries-dir $(PB_FDA_FLAT_INTERMEDIARES) \
		--hartmann-priors $(PB_HARTMANN_PRIORS) \
		--hartmann-intermediaries-dir $(PB_FDA_HARTMANN_INTERMEDIARES) \
		--output $@

$(PB_POP_PRIORS_POSTERIORS_PLOT) \
$(PB_COV_PRIORS_POSTERIORS_PLOT) \
$(PB_SMALL_COV_PRIORS_POSTERIORS_PLOT) &: \
	$(PB_SCRIPTS)/plot-compare-prior-posterior.R \
	$(PB_ALL_PRIORS_AND_POSTERIORS_COMPARE)
	$(RSCRIPT) $< \
		--prior-posterior-compare $(PB_ALL_PRIORS_AND_POSTERIORS_COMPARE) \
		--output-pop-plot $(PB_POP_PRIORS_POSTERIORS_PLOT) \
		--output-cov-plot $(PB_COV_PRIORS_POSTERIORS_PLOT) \
		--output-small-cov-plot $(PB_SMALL_COV_PRIORS_POSTERIORS_PLOT) \
		--output ""

# ============================= tuning parameters ==============================
# evaluation tuning
TUNING_BASENAME = tuning-parameters
TUNING_RDS = $(RDS)/$(TUNING_BASENAME)
TUNING_SCRIPTS = $(SCRIPTS)/$(TUNING_BASENAME)
TUNING_PLOTS = $(PLOTS)/$(TUNING_BASENAME)

## rds
TUNING_VAR_AT_OPTIMA_RESULTS = $(TUNING_RDS)/variance-at-optima.rds

## plots
TUNING_VAR_AT_OPTIMA_PLOT = $(TUNING_PLOTS)/variance-at-optima.pdf
ALL_PLOTS += $(TUNING_VAR_AT_OPTIMA_PLOT)

## rules
$(TUNING_VAR_AT_OPTIMA_RESULTS) : \
	$(TUNING_SCRIPTS)/run-variance-at-optima.R \
	$(PB_GLOBALS) \
	$(PB_POPULATION_ALL_ROWS)
	$(RSCRIPT) $< \
		--population-intermediaries-dir $(PB_POPULATION_INTERMEDIARES) \
		--output $@

$(TUNING_VAR_AT_OPTIMA_PLOT) : \
	$(TUNING_SCRIPTS)/plot-variance-at-optima.R \
	$(TUNING_VAR_AT_OPTIMA_RESULTS)
	$(RSCRIPT) $< \
		--variance-at-optima-tbl $(TUNING_VAR_AT_OPTIMA_RESULTS) \
		--output $@

## batching tuning
BATCHING_BASENAME = batching-and-optimiser-tests
BATCHING_RDS = $(RDS)/$(BATCHING_BASENAME)
BATCHING_SCRIPTS = $(SCRIPTS)/$(BATCHING_BASENAME)
BATCHING_PLOTS = $(PLOTS)/$(BATCHING_BASENAME)
BATCHING_INTERMEDIARIRES = $(BATCHING_RDS)/intermediaries

## rds
BATCHING_ALL_TASK_RESULTS = $(wildcard $(BATCHING_INTERMEDIARIRES)/task-*.rds)
BATCHING_PLOT_TBLS = $(BATCHING_RDS)/all-batching-optim-plot-dfs.rds

#a plots
BATCHING_FULL_PLOT = $(BATCHING_PLOTS)/fullpage-batching-tests-plot.pdf
ALL_PLOTS += $(BATCHING_FULL_PLOT)

## rules
$(BATCHING_PLOT_TBLS) : \
	$(BATCHING_SCRIPTS)/process-output-for-plotting.R \
	$(BATCHING_ALL_TASK_RESULTS)
	$(RSCRIPT) $< \
		--batching-intermediaries-dir $(BATCHING_INTERMEDIARIRES) \
		--output $@

$(BATCHING_FULL_PLOT) : \
	$(BATCHING_SCRIPTS)/plot-all-results.R \
	$(BATCHING_PLOT_TBLS)
	$(RSCRIPT) $< \
		--batching-plot-tbls $(BATCHING_PLOT_TBLS) \
		--output $@

# =============================== ideal process ================================
IDEAL_PROCESS_PLOT = $(TUNING_PLOTS)/idealised-process.pdf
ALL_PLOTS += $(IDEAL_PROCESS_PLOT)

$(IDEAL_PROCESS_PLOT) : \
	$(TUNING_SCRIPTS)/plot-idealised-process.R
	$(RSCRIPT) $< \
		--output $@

# =================================== readme ===================================
## readme files
README_BASENAME = readme
README_RDS = $(RDS)/$(README_BASENAME)
README_SCRIPTS = $(SCRIPTS)/$(README_BASENAME)
README_PLOTS = $(PLOTS)/$(README_BASENAME)
README_INTERMEDIARIRES = $(README_RDS)/intermediaries

## rds
README_SIMPLE_LAMBDA = $(README_RDS)/simple-lambda-est.rds

## plots
README_SIMPLE_PLOT = $(README_PLOTS)/simple-obs-plot.pdf
README_HIER_OBS_PLOT = $(README_PLOTS)/hierarchical-obs.pdf
README_HIER_THETA_PLOT = $(README_PLOTS)/hierarchical-theta.pdf

ALL_PLOTS += $(README_SIMPLE_PLOT) $(README_HIER_OBS_PLOT) $(README_HIER_THETA_PLOT)

## rules
$(README_SIMPLE_LAMBDA) \
$(README_SIMPLE_PLOT) \
$(README_HIER_OBS_PLOT) \
$(README_HIER_THETA_PLOT) &: \
	$(README_SCRIPTS)/plot-readme.R
	$(RSCRIPT) $< \
		--simple-lambda-output $(README_SIMPLE_LAMBDA) \
		--simple-plot-output $(README_SIMPLE_PLOT) \
		--hier-obs-plot-output $(README_HIER_OBS_PLOT) \
		--hier-theta-plot-output $(README_HIER_THETA_PLOT) \
		--output ""

# ================================ output rule =================================
# knitr is becoming more picky about encoding, specify UTF-8 input
$(WRITEUP) : $(wildcard *.rmd) $(TEX_FILES) $(ALL_PLOTS)
	$(RSCRIPT) -e "rmarkdown::render(input = Sys.glob('$(BASENAME).rmd'), encoding = 'UTF-8')"
